<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div id="nav">
    <h3>${title}</h3>
    <ul>  
        <li class="first"><a href="<c:url value="/home" />" > Home</a></li>
        <sec:authorize access="isAuthenticated()">
            <li class=""><a href="<c:url value="/track" />" > Tracks</a></li>
            <li class="last"><a href="<c:url value="/j_spring_security_logout" />" > Logout</a></li>
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
            <li class=""><a href="<c:url value="/login" />" > Login</a></li>
            <li class="last"><a href="<c:url value="/register" />" > Register</a></li>
        </sec:authorize>   
    </ul>
</div>