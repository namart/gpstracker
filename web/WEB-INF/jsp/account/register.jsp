<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>




<form:form method="POST" commandName="registration">
        <form:errors path="*" cssClass="errorblock" element="div" />
        
        <label for="username">Username</label>
        <form:input path="username" />
        <form:errors path="username" cssClass="error" />
        

        <label for="password">Password</label>
        <form:input path="password" type="password" />
        <form:errors path="password" cssClass="error" />
        

        <label for="confirmPassword">Password</label>
        <form:input path="confirmPassword" type="password" />
        <form:errors path="confirmPassword" cssClass="error" />
        

        <label for="email">Email</label>
        <form:input path="email" />
        <form:errors path="email" cssClass="error" />
        

        <button type="submit">Register</button>

</form:form>