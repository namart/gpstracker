<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>


    <c:if test="${not empty error}">
        <div class="errorblock">
                Your login attempt was not successful, try again.<br /> Caused :
                ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
        </div>
    </c:if>
    <form name='f' action="<c:url value='j_spring_security_check' />" method='POST'>
            <label for="username">Username</label>
            <input type='text' id="username" name='j_username' value=''>
            
            <label for="password">Password</label>
            <input type='password' id="password" name='j_password' />

            <fieldset>
            <label for="rememberMe">
                    <input type="checkbox" name="_spring_security_remember_me" id="rememberMe">
                    <span>Remember me</span>
            </label>
            </fieldset>

            <button type="submit">Login</button>
    </form>
