<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<script type="text/javascript">
    var waypoints = [
        <c:forEach items="${waypoints}" var="waypoint" varStatus="status">
         { "latitude" : ${waypoint.latitude}, "longitude" : ${waypoint.longtitude}} ${not status.last ? ',' : ''}                    
        </c:forEach>
    ];
    
    $(function() {
        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();
        var map;
        
        directionsDisplay = new google.maps.DirectionsRenderer();
        var startWaypoint = new google.maps.LatLng(${start.latitude}, ${start.longtitude});
        var endWaypoint = new google.maps.LatLng(${end.latitude}, ${end.longtitude});
        var myOptions = {
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: startWaypoint
        }
        
        var waypts = [];
        for (var i = 0; i < waypoints.length; i++) {

            waypts.push({
                location:new google.maps.LatLng(waypoints[i].latitude,waypoints[i].longitude),
                stopover:true});

        }
    
        var request = {
            origin: startWaypoint, 
            destination: endWaypoint,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.${track.mode.name}
        };
        
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                directionsDisplay.setMap(map);
              }
            });
     
    });
    
</script>

<div id="route-map" class="twelve columns alpha" style="height:400px;">
    <div id="map_canvas"></div>
</div>
<div class="four columns omega">
    
    <p><h5>Description: </h4>${track.description}</p>
    <p><h5>Average speed: </h4>${track.avg_speed}</p>
    <p><h5>Distance: </h4>${track.distance}</p>
    <p><h5>Date: </h4>${track.end_date}</p>
</div>

