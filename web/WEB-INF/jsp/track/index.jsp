<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div id="track-list">
    
    <span class="sort button" data-sort="name">Sort by name</span>
    <span class="sort button" data-sort="end-date">Sort by date</span>
    <span class="sort button" data-sort="distance">Sort by distance</span>
    <span class="sort button" data-sort="avg-speed">Sort by average speed</span>
    

    <ul class="list">
        <c:forEach items="${tracks}" var="track" varStatus="stat">
           <li class="tracklist_item<c:if test="${stat.count % 2 == 0}">_even</c:if>">
              <a href="<c:url value="/track/${track.id}" />">
                <h4><span class="name">${track.name}</span></h4></h4>
                <p class="description">${track.description}</p>
                <h6>Distance <span class="distance">${track.distance}</span></h6>
                <h6>Average speed <span class="avg-speed">${track.avg_speed}</span></h6>
                <h6>End date <span class="end-date">${track.end_date}</span></h6>
              </a>
            </li>
        </c:forEach>
    </ul>
</div>

<script type="text/javascript">
var options = {
    valueNames: [ 'name', 'end-date', 'distance', 'avg-speed'  ]
};

var hackerList = new List('track-list', options);
    
</script>

