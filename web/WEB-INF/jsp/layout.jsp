<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><tiles:insertAttribute name="title" ignore="true" /></title>
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="<c:url value='/static/stylesheets/base.css'/>">
    <link rel="stylesheet" href="<c:url value='/static/stylesheets/skeleton.css'/>">
    <link rel="stylesheet" href="<c:url value='/static/stylesheets/layout.css'/>">
    <link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
    
    <script src="<c:url value='/static/javascripts/jquery-1.7.1.min.js'/>"></script>
    <script src="<c:url value='/static/javascripts/tabs.js'/>"></script>
    <script src="<c:url value='/static/javascripts/list.min.js'/>"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>

</head>
<body>
    <div class="container">
        <div class="sixteen columns clearfix">
            <div class="six columns alpha"><tiles:insertAttribute name="header" /></div>
        </div>
        <div class="sixteen columns">
            <tiles:insertAttribute name="menu" />
        </div>
        <div class="sixteen columns">
            <tiles:insertAttribute name="body" />
        </div>
        <div class="sixteen columns footer">
            <tiles:insertAttribute name="footer" />
        </div>
    </div>
</body>
</html>