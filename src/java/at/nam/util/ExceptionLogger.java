/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.util;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 *
 * @author nam
 */

@Aspect
public class ExceptionLogger {
    
    private static final Logger LOGGER = Logger.getLogger(ExceptionLogger.class);
    
    /*
    
    @Before("within(at.nam.bo..*)")
    public void boExceptionExecution() {
        System.out.println("Biúsiness layer thowed an before");
    }
    
    @Before("within(at.nam.dao..*)")
    public void dalExecution() {
        System.out.println("DAL thowed an before");
    }
     */
    
    @AfterThrowing(
      pointcut = "within(at.nam.bo..*)",
      throwing= "error")
    public void boExceptionExecution(JoinPoint joinPoint, Throwable error) {
        
        System.out.println("BO throwed exception");
        
        StringBuffer logMessageStringBuffer = new StringBuffer();  
        logMessageStringBuffer.append(joinPoint.getTarget().getClass().getName());  
        logMessageStringBuffer.append(".");  
        logMessageStringBuffer.append(joinPoint.getSignature().getName());  
        logMessageStringBuffer.append("(");  
        logMessageStringBuffer.append(joinPoint.getArgs());  
        logMessageStringBuffer.append(")"); 
        logMessageStringBuffer.append(" Exception: " + error);  
        
        LOGGER.debug(logMessageStringBuffer.toString());
        
    }
   
    @AfterThrowing(
      pointcut = "within(at.nam.dao..*)",
      throwing= "error")
    public void dalExceptionExecution(JoinPoint joinPoint, Throwable error) {
        
        System.out.println("DAL throwed exception");
        
        StringBuffer logMessageStringBuffer = new StringBuffer();  
        logMessageStringBuffer.append(joinPoint.getTarget().getClass().getName());  
        logMessageStringBuffer.append(".");  
        logMessageStringBuffer.append(joinPoint.getSignature().getName());  
        logMessageStringBuffer.append("(");  
        logMessageStringBuffer.append(joinPoint.getArgs());  
        logMessageStringBuffer.append(")"); 
        logMessageStringBuffer.append(" Exception: " + error);  
        
        LOGGER.debug(logMessageStringBuffer.toString());
    }
    
}
