/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.exception;

import at.nam.dao.ModeDao;
import at.nam.model.Mode;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */

public class ModeDaoImplException implements ModeDao {

    @Override
    public Mode save(Mode eo) {
        throw new DataAccessException("error adding mode") {};
    }

    @Override
    public void update(Mode eo) {
        throw new DataAccessException("error updating mode") {};
    }

    @Override
    public void delete(Mode eo) {
        throw new DataAccessException("error deleting mode") {};
    }

    @Override
    public List<Mode> getAll() {
        throw new DataAccessException("error getting modi") {};
    }

    @Override
    public Mode get(Integer id) {
        throw new DataAccessException("error getting mode") {};
    }
    
}
