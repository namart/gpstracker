/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.exception;

import at.nam.dao.RoleDao;
import at.nam.model.Role;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */

public class RoleDaoImplException implements RoleDao {

    @Override
    public Role save(Role eo) {
        throw new DataAccessException("error adding role") {};
    }

    @Override
    public void update(Role eo) {
        throw new DataAccessException("error updating role") {};
    }

    @Override
    public void delete(Role eo) {
        throw new DataAccessException("error deleting role") {};
    }

    @Override
    public List<Role> getAll() {
        throw new DataAccessException("error getting roles") {};
    }

    @Override
    public Role get(Integer id) {
        throw new DataAccessException("error getting role") {};
    }
    
}
