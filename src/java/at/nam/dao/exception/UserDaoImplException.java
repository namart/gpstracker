/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.exception;

import at.nam.dao.UserDao;
import at.nam.model.User;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */

public class UserDaoImplException implements UserDao {

    @Override
    public User save(User eo) {
        throw new DataAccessException("error adding user") {};
    }

    @Override
    public void update(User eo) {
        throw new DataAccessException("error updating user") {};
    }

    @Override
    public void delete(User eo) {
        throw new DataAccessException("error deleting user") {};
    }

    @Override
    public List<User> getAll() {
        throw new DataAccessException("error getting users") {};
    }

    @Override
    public User get(Integer id) {
        throw new DataAccessException("error getting user") {};
    }

    @Override
    public User getUserByUsernameAndPassword(String email, String password) {
        throw new DataAccessException("error getting user") {};
    }

    @Override
    public User getUserByUsername(String username) {
        throw new DataAccessException("error getting user") {};
    }
    
}
