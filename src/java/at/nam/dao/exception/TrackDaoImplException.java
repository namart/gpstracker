/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.exception;

import at.nam.dao.TrackDao;
import at.nam.model.Track;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */

public class TrackDaoImplException implements TrackDao {

    @Override
    public Track save(Track eo) {
        throw new DataAccessException("error adding track") {};
    }

    @Override
    public void update(Track eo) {
        throw new DataAccessException("error updating track") {};
    }

    @Override
    public void delete(Track eo) {
        throw new DataAccessException("error deleting track") {};
    }

    @Override
    public List<Track> getAll() {
        throw new DataAccessException("error getting tracks") {};
    }

    @Override
    public Track get(Integer id) {
        throw new DataAccessException("error getting track") {};
    }

    @Override
    public List<Track> getTracksByUserId(Integer userId) {
        throw new DataAccessException("error getting tracks") {};
    }
    
}
