/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.exception;

import at.nam.dao.WaypointDao;
import at.nam.model.Waypoint;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */

public class WaypointDaoImplException implements WaypointDao {

    @Override
    public Waypoint save(Waypoint eo) {
        throw new DataAccessException("error adding waypoint") {};
    }

    @Override
    public void update(Waypoint eo) {
        throw new DataAccessException("error updating waypoint") {};
    }

    @Override
    public void delete(Waypoint eo) {
        throw new DataAccessException("error deleting waypoint") {};
    }

    @Override
    public List<Waypoint> getAll() {
        throw new DataAccessException("error getting waypoint") {};
    }

    @Override
    public Waypoint get(Integer id) {
        throw new DataAccessException("error getting waypoint") {};
    }

    @Override
    public List<Waypoint> getWaypointsByTrackId(Integer trackId) {
        throw new DataAccessException("error getting waypoint") {};
    }
    
}
