/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao;

import org.springframework.dao.DataAccessException;

/**
 *
 * @author nam
 */
public class DALException extends DataAccessException {
    
    public DALException(String msg) {
		super(msg);
	}

	public DALException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
