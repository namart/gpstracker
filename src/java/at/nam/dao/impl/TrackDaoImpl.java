/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.impl;

import at.nam.dao.TrackDao;
import at.nam.model.Track;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nam
 */

@Repository
@Transactional
public class TrackDaoImpl implements TrackDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Track save(Track eo) {
        sessionFactory.getCurrentSession().saveOrUpdate(eo); 
        return eo;
    }

    @Override
    public void update(Track eo) {
        sessionFactory.getCurrentSession().update(eo);
    }

    @Override
    public void delete(Track eo) {
        sessionFactory.getCurrentSession().delete(eo);
    }

    @Override
    public List<Track> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Track").list();
    }

    @Override
    public Track get(Integer id) {
        return (Track) sessionFactory.getCurrentSession().get(Track.class, id);
    }

    @Override
    public List<Track> getTracksByUserId(Integer userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Track where user_id = :userId ");
        query.setParameter("userId", userId);
        return query.list();
        
    }
    
}
