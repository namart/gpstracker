/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.impl;

import at.nam.dao.RoleDao;
import at.nam.model.Role;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nam
 */

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Role save(Role eo) {
        sessionFactory.getCurrentSession().saveOrUpdate(eo); 
        return eo;
    }

    @Override
    public void update(Role eo) {
        sessionFactory.getCurrentSession().update(eo);
    }

    @Override
    public void delete(Role eo) {
        sessionFactory.getCurrentSession().delete(eo);
    }

    @Override
    public List getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Role").list();
    }

    @Override
    public Role get(Integer id) {
        return (Role) sessionFactory.getCurrentSession().get(Role.class, id);
    }
    
}
