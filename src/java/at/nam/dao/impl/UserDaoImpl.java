/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.impl;

import at.nam.dao.UserDao;
import at.nam.model.User;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nam
 */

@Repository
@Transactional
public class UserDaoImpl implements UserDao {
    
    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public User save(User eo) {
        sessionFactory.getCurrentSession().saveOrUpdate(eo); 
        return eo;
    }

    @Override
    public void update(User eo) {
        sessionFactory.getCurrentSession().update(eo);
    }

    @Override
    public void delete(User eo) {
        sessionFactory.getCurrentSession().delete(eo);
    }

    @Override
    public List<User> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    @Override
    public User get(Integer id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {

        Query query = sessionFactory.getCurrentSession().createQuery("from User where username = :username and password = :password ");
        query.setParameter("username", username);
        query.setParameter("password", password);
        List<User> list = query.list();
        
        if(list.size() >= 1) {
            return list.get(0);
        } else {
            return null;
        }
        
    }

    @Override
    public User getUserByUsername(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User where username = :username ");
        query.setParameter("username", username);
        List<User> list = query.list();
        
        if(list.size() >= 1) {
            return list.get(0);
        } else {
            return null;
        }
    }
    
}
