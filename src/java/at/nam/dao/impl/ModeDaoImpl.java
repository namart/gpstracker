/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.impl;

import at.nam.dao.ModeDao;
import at.nam.model.Mode;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nam
 */

@Repository
@Transactional
public class ModeDaoImpl implements ModeDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Mode save(Mode eo) {
        sessionFactory.getCurrentSession().saveOrUpdate(eo); 
        return eo;
    }

    @Override
    public void update(Mode eo) {
        sessionFactory.getCurrentSession().update(eo);
    }

    @Override
    public void delete(Mode eo) {
        sessionFactory.getCurrentSession().delete(eo);
    }

    @Override
    public List<Mode> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Mode").list();
    }

    @Override
    public Mode get(Integer id) {
        return (Mode) sessionFactory.getCurrentSession().get(Mode.class, id);
    }
    
}
