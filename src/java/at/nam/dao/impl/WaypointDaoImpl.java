/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.impl;

import at.nam.dao.WaypointDao;
import at.nam.model.Waypoint;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author nam
 */

@Repository
@Transactional
public class WaypointDaoImpl implements WaypointDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Waypoint save(Waypoint eo) {
        sessionFactory.getCurrentSession().saveOrUpdate(eo); 
        return eo;
    }

    @Override
    public void update(Waypoint eo) {
        sessionFactory.getCurrentSession().update(eo);
    }

    @Override
    public void delete(Waypoint eo) {
        sessionFactory.getCurrentSession().delete(eo);
    }

    @Override
    public List<Waypoint> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Waypoint").list();
    }

    @Override
    public Waypoint get(Integer id) {
        return (Waypoint) sessionFactory.getCurrentSession().get(Waypoint.class, id);
    }

    @Override
    public List<Waypoint> getWaypointsByTrackId(Integer trackId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Waypoint where track_id = :trackId ");
        query.setParameter("trackId", trackId);
        return query.list();
    }
    
}
