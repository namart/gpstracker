/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao;

import at.nam.model.Track;
import java.util.List;

/**
 *
 * @author nam
 */
public interface TrackDao extends DefaultDao<Track> {
    
    List<Track> getTracksByUserId(Integer userId);
    
}
