/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao;

import at.nam.model.Waypoint;
import java.util.List;

/**
 *
 * @author nam
 */
public interface WaypointDao extends DefaultDao<Waypoint> {
    
    List<Waypoint> getWaypointsByTrackId(Integer trackId);
    
}
