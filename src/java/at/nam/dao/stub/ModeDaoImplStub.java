/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.stub;

import at.nam.dao.ModeDao;
import at.nam.model.Mode;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author nam
 */

public class ModeDaoImplStub implements ModeDao {
    
    private List<Mode> currentSessions =new ArrayList<Mode>();
    private int idCount = 0;

    @Override
    public Mode save(Mode eo) {
        eo.setId( ++this.idCount );
        this.currentSessions.add(eo);
        return eo;
    }

    @Override
    public void update(Mode eo) {
        for (int i = 0; i < this.currentSessions.size(); i++) {
            if(this.currentSessions.get(i).getId() == eo.getId()) {
                this.currentSessions.add(i, eo);
            }
            
        }
    }

    @Override
    public void delete(Mode eo) {
        for(Mode u : this.currentSessions) {
            if(u.getId() == eo.getId()) {
                this.currentSessions.remove(u);
            }
        }
    }

    @Override
    public List<Mode> getAll() {
        return this.currentSessions;
    }

    @Override
    public Mode get(Integer id) {
        for(Mode u : this.currentSessions) {
            if(u.getId() == id) {
                return u;
            }
        }

	return null;
    }
    
}
