/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.stub;

import at.nam.dao.RoleDao;
import at.nam.model.Role;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nam
 */

public class RoleDaoImplStub implements RoleDao {
    
    private List<Role> currentSessions =new ArrayList<Role>();
    private int idCount = 0;

    @Override
    public Role save(Role eo) {
        eo.setId( ++this.idCount );
        this.currentSessions.add(eo);
        return eo;
    }

    @Override
    public void update(Role eo) {
        for (int i = 0; i < this.currentSessions.size(); i++) {
            if(this.currentSessions.get(i).getId() == eo.getId()) {
                this.currentSessions.add(i, eo);
            }
            
        }
    }

    @Override
    public void delete(Role eo) {
        for(Role u : this.currentSessions) {
            if(u.getId() == eo.getId()) {
                this.currentSessions.remove(u);
            }
        }
    }

    @Override
    public List<Role> getAll() {
        return this.currentSessions;
    }

    @Override
    public Role get(Integer id) {
        for(Role u : this.currentSessions) {
            if(u.getId() == id) {
                return u;
            }
        }

	return null;
    }
    
}
