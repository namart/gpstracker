/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.stub;

import at.nam.dao.WaypointDao;
import at.nam.model.Waypoint;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nam
 */

public class WaypointDaoImplStub implements WaypointDao {
    
    private List<Waypoint> currentSessions =new ArrayList<Waypoint>();
    private int idCount = 0;

    @Override
    public Waypoint save(Waypoint eo) {
        eo.setId( ++this.idCount );
        this.currentSessions.add(eo);
        return eo;
    }

    @Override
    public void update(Waypoint eo) {
        for (int i = 0; i < this.currentSessions.size(); i++) {
            if(this.currentSessions.get(i).getId() == eo.getId()) {
                this.currentSessions.add(i, eo);
            }
            
        }
    }

    @Override
    public void delete(Waypoint eo) {
        for(Waypoint u : this.currentSessions) {
            if(u.getId() == eo.getId()) {
                this.currentSessions.remove(u);
            }
        }
    }

    @Override
    public List<Waypoint> getAll() {
        return this.currentSessions;
    }

    @Override
    public Waypoint get(Integer id) {
        for(Waypoint u : this.currentSessions) {
            if(u.getId() == id) {
                return u;
            }
        }

	return null;
    }

    @Override
    public List<Waypoint> getWaypointsByTrackId(Integer trackId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
