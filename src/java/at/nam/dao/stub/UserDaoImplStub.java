/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.stub;

import at.nam.dao.UserDao;
import at.nam.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nam
 */

public class UserDaoImplStub implements UserDao {
    
    private List<User> currentSessions =new ArrayList<User>();
    private int idCount = 0;

    @Override
    public User save(User eo) {
        eo.setId( ++this.idCount );
        this.currentSessions.add(eo);
        return eo;
    }

    @Override
    public void update(User eo) {
        for (int i = 0; i < this.currentSessions.size(); i++) {
            if(this.currentSessions.get(i).getId() == eo.getId()) {
                this.currentSessions.add(i, eo);
            }
            
        }
    }

    @Override
    public void delete(User eo) {
        for(User u : this.currentSessions) {
            if(u.getId() == eo.getId()) {
                this.currentSessions.remove(u);
            }
        }
    }

    @Override
    public List<User> getAll() {
        return this.currentSessions;
    }

    @Override
    public User get(Integer id) {
        for(User u : this.currentSessions) {
            if(u.getId() == id) {
                return u;
            }
        }

	return null;
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        for(User u : this.currentSessions) {
            if (username.equals(u.getUsername()) && password.equals(u.getPassword())) {
                return u;
            }
        }
        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        for(User u : this.currentSessions) {
            if (username.equals(u.getUsername())) {
                return u;
            }
        }
        return null;
    }
    
}
