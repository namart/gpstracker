/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao.stub;

import at.nam.dao.TrackDao;
import at.nam.model.Track;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author nam
 */

public class TrackDaoImplStub implements TrackDao {
    
    private List<Track> currentSessions =new ArrayList<Track>();
    private int idCount = 0;

    @Override
    public Track save(Track eo) {
        eo.setId( ++this.idCount );
        this.currentSessions.add(eo);
        return eo;
    }

    @Override
    public void update(Track eo) {
        for (int i = 0; i < this.currentSessions.size(); i++) {
            if(this.currentSessions.get(i).getId() == eo.getId()) {
                this.currentSessions.add(i, eo);
            }
            
        }
    }

    @Override
    public void delete(Track eo) {
        for(Track u : this.currentSessions) {
            if(u.getId() == eo.getId()) {
                this.currentSessions.remove(u);
            }
        }
    }

    @Override
    public List<Track> getAll() {
        return this.currentSessions;
    }

    @Override
    public Track get(Integer id) {
        for(Track u : this.currentSessions) {
            if(u.getId() == id) {
                return u;
            }
        }

	return null;
    }

    @Override
    public List<Track> getTracksByUserId(Integer userId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
