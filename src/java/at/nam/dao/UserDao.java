/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.dao;

import at.nam.model.User;

/**
 *
 * @author nam
 */
public interface UserDao extends DefaultDao<User> {
    
    User getUserByUsernameAndPassword(String email, String password);
    User getUserByUsername(String username);
}
