/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author nam
 */

@Entity
@Table( name = "tracks" )
public class Track {
    
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;
    
    @NotNull
    private String name;
    private String description;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date start_date;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end_date;
    private boolean is_public;
    private int distance;
    private int avg_speed;
    
    @ManyToOne
    private Waypoint last_waypoint;

    @ManyToOne
    private User user;
    
    @ManyToOne
    private Mode mode;
    
    public Track() {

    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the start_date
     */
    public Date getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public Date getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the is_public
     */
    public boolean isIs_public() {
        return is_public;
    }

    /**
     * @param is_public the is_public to set
     */
    public void setIs_public(boolean is_public) {
        this.is_public = is_public;
    }

    /**
     * @return the distance
     */
    public int getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * @return the avg_speed
     */
    public int getAvg_speed() {
        return avg_speed;
    }

    /**
     * @param avg_speed the avg_speed to set
     */
    public void setAvg_speed(int avg_speed) {
        this.avg_speed = avg_speed;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the last_waypoint
     */
    public Waypoint getLast_waypoint() {
        return last_waypoint;
    }

    /**
     * @param last_waypoint the last_waypoint to set
     */
    public void setLast_waypoint(Waypoint last_waypoint) {
        this.last_waypoint = last_waypoint;
    }

    /**
     * @return the mode
     */
    public Mode getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(Mode mode) {
        this.mode = mode;
    }
       
    
}
