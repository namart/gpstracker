/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author nam
 */

@Entity
@Table( name = "modi" )
public class Mode {
    
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    private String name;
    private int max_speed;
    
    public Mode() {
        
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the max_speed
     */
    public int getMax_speed() {
        return max_speed;
    }

    /**
     * @param max_speed the max_speed to set
     */
    public void setMax_speed(int max_speed) {
        this.max_speed = max_speed;
    }
    
    
    
}
