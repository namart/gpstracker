/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nam
 */

@Entity
@Table( name = "waypoints" )
public class Waypoint {
    
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Integer id;

    private double latitude;
    private double longtitude;

    @Temporal(TemporalType.TIMESTAMP)
    private Date capture_speed;
    private int speed;

    @ManyToOne
    private Track track;
    
    public Waypoint() {

    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longtitude
     */
    public double getLongtitude() {
        return longtitude;
    }

    /**
     * @param longtitude the longtitude to set
     */
    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    /**
     * @return the capture_speed
     */
    public Date getCapture_speed() {
        return capture_speed;
    }

    /**
     * @param capture_speed the capture_speed to set
     */
    public void setCapture_speed(Date capture_speed) {
        this.capture_speed = capture_speed;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the track
     */
    public Track getTrack() {
        return track;
    }

    /**
     * @param track the track to set
     */
    public void setTrack(Track track) {
        this.track = track;
    }
    
    
}
