/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author nam
 */

@Entity
@Table( name = "users" )
public class User {
    
    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(name = "username", nullable = false, length = 20, unique = true)
    private String username;
	
    @NotNull
    @Column(name = "password", nullable = false, length = 20)
    private String password;
	 
    @NotNull
    @Column(name = "email", nullable = false, length = 20)
    private String email;
    
    @NotNull
    @Column(name = "enabled", nullable = true)
    private Boolean enabled;
    
    public User() {
        // this form used by Hibernate
    }

    public User(String username, String password, String email, Boolean enabled) {
        // for application use, to create new users
        super();
        this.username = username;
        this.password = password;
        this.email = email;
        this.enabled = enabled;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the enabled
     */
    public Boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    
}
