/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.webservice;

import at.nam.model.Mode;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import at.nam.model.User;
import at.nam.model.Waypoint;
import java.util.List;

/**
 *
 * @author nam
 */

@WebService
public interface GPSWebService {
    
    @WebMethod(operationName = "newTrack")
    Integer newTrack(@WebParam(name = "username") String username, @WebParam(name = "password")String password, @WebParam(name = "trackName") String trackName, @WebParam(name = "modeId") Integer modeId);
   
    @WebMethod(operationName = "newWaypoint")
    void newWayPoint(@WebParam(name = "username") String username, @WebParam(name = "password")String password, @WebParam(name = "trackId") Integer trackId, @WebParam(name = "latitude") double latitude, @WebParam(name = "longtitude") double longtitude);
    
    @WebMethod(operationName = "endTrack")
    Integer endTrack(@WebParam(name = "username") String username, @WebParam(name = "password")String password, @WebParam(name = "trackId") Integer trackId);
    
    @WebMethod(operationName = "getModi")
    List<Mode> getModi();
    
}
