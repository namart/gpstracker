/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.webservice;

import at.nam.bo.ModeBo;
import at.nam.bo.TrackBo;
import at.nam.bo.UserBo;
import at.nam.bo.WaypointBo;
import at.nam.model.Mode;
import at.nam.model.Track;
import at.nam.model.User;
import at.nam.model.Waypoint;
import java.util.Date;
import javax.jws.WebService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


/**
 *
 * @author nam
 */

@WebService(serviceName = "WebService", endpointInterface = "at.nam.webservice.GPSWebService")
@Service("WebServiceEndpoint")
public class GPSWebServiceImpl extends SpringBeanAutowiringSupport implements GPSWebService {
 
    @Autowired
    private UserBo userBo;
    
    @Autowired
    private TrackBo trackBo;
    
    @Autowired
    private WaypointBo waypointBo;
    
    @Autowired
    private ModeBo modeBo;

    @Override
    public Integer newTrack(String username, String password, String trackName, Integer modeId) {
        
        User user = userBo.AuthenticateUser(username, password);
        Mode mode = modeBo.get(modeId);
        if (user != null && mode != null) {
            
            Track track = new Track();
            track.setName(trackName);
            track.setUser(user);
            track.setStart_date(new Date());
            track.setMode(mode);
            trackBo.save(track);
            
            return track.getId();
            
        } else {
            return 0;
        }
    }
    
    @Override
    public void newWayPoint(String username, String password, Integer trackId, double latitude, double longtitude) {
        User user = userBo.AuthenticateUser(username, password);
        if (user != null) {
            waypointBo.newWaypoint(trackId, latitude, longtitude);
        } 
    }
    
    @Override
    public Integer endTrack(String username, String password, Integer trackId) {
        User user = userBo.AuthenticateUser(username, password);
        if (user != null) {

            trackBo.endTrack(trackId);
            
            return 1;
            
        } else {
            return 0;
        }
    }

    @Override
    public List<Mode> getModi() {
        return modeBo.getAll();
    }

}
