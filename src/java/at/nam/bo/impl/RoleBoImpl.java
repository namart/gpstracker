/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.impl;

import at.nam.bo.RoleBo;
import at.nam.dao.RoleDao;
import at.nam.model.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nam
 */

@Service()
public class RoleBoImpl implements RoleBo {

    @Autowired
    RoleDao roleDao;
    
    @Override
    public Role save(Role role) {
        return roleDao.save(role);
    }

    @Override
    public void update(Role role) {
        roleDao.update(role);
    }

    @Override
    public void delete(Role role) {
        roleDao.delete(role);
    }

    @Override
    public List<Role> getAll() {
        return roleDao.getAll();
    }

    @Override
    public Role get(Integer id) {
        return roleDao.get(id);
    }
    
}
