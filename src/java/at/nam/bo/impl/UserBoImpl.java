/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.impl;

import at.nam.bo.UserBo;
import at.nam.dao.UserDao;
import at.nam.model.User;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import javax.validation.Validator;



/**
 *
 * @author nam
 */

@Service()
public class UserBoImpl implements UserBo {
    
    @Autowired
    UserDao userDao;
    
    @Autowired
    Validator validator;

    @Override
    public User save(User user) {
        
        
        Set<ConstraintViolation<User>> contraintViolations = validator.validate(user);
        System.out.print(contraintViolations);
                
        return userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(User user) {
        userDao.delete(user);
    }

    @Override
    public List getAll() {
        return userDao.getAll();
    }

    @Override
    public User get(Integer id) {
        return userDao.get(id);
    }

    @Override
    public User AuthenticateUser(String username, String password) {
        return userDao.getUserByUsernameAndPassword(username, password);   
    }

    @Override
    public Integer getUserIdByUsername(String username) {
        User user = userDao.getUserByUsername(username);
        if(user != null){
            return user.getId();
        } else {
            return null;
        }
    }
    
}
