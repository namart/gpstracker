/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.impl;

import at.nam.bo.WaypointBo;
import at.nam.dao.ModeDao;
import at.nam.dao.TrackDao;
import at.nam.dao.WaypointDao;
import at.nam.model.Mode;
import at.nam.model.Track;
import at.nam.model.Waypoint;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nam
 */

@Service()
public class WaypointBoImpl implements WaypointBo {
    
    @Autowired
    WaypointDao waypointDao;
    
    @Autowired
    TrackDao trackDao;
    
    @Autowired
    ModeDao modeDao;

    @Override
    public Waypoint save(Waypoint eo) {
        return waypointDao.save(eo);
    }

    @Override
    public void update(Waypoint eo) {
        waypointDao.update(eo);
    }

    @Override
    public void delete(Waypoint eo) {
        waypointDao.delete(eo);
    }

    @Override
    public List<Waypoint> getAll() {
        return waypointDao.getAll();
    }

    @Override
    public Waypoint get(Integer id) {
        return waypointDao.get(id);
    }

    @Override
    public void newWaypoint(Integer trackId, double latitude, double longtitude) {
        
        Track track = trackDao.get(trackId);
        
        if (track != null) {
            
            Waypoint lastWaypoint = track.getLast_waypoint();
            Waypoint newWaypoint = new Waypoint();
            newWaypoint.setLatitude(latitude);
            newWaypoint.setLongtitude(longtitude);
            newWaypoint.setTrack(track);
            newWaypoint.setCapture_speed(new Date());
            
            
            if (lastWaypoint == null) {
                
                //save new waypoint
                newWaypoint.setSpeed(0);
                waypointDao.save(newWaypoint);
                
                //save new/las waypoint
                track.setLast_waypoint(newWaypoint);
                trackDao.save(track);
            } else {
                
                //Missing: Check if latitude&longtitude out of border
                
                double waypointsDistanceInKilo = this.distanceInK(lastWaypoint.getLatitude(), lastWaypoint.getLongtitude(), latitude, longtitude);
                Date today = new Date();
                long diffInMill = today.getTime() - lastWaypoint.getCapture_speed().getTime();
                float diffInHour = ((float)(today.getTime() - lastWaypoint.getCapture_speed().getTime())) / (1000 * 60 * 60);
                Integer speed = (int)(waypointsDistanceInKilo/diffInHour);
                System.out.println("today: " + today.getTime());
                System.out.println("last waypoint capture speed: " + lastWaypoint.getCapture_speed().getTime());
                System.out.println("substrackt&cal: " + (today.getTime() - lastWaypoint.getCapture_speed().getTime()));
                System.out.println("distance: " + waypointsDistanceInKilo);
                System.out.println("hour: " + diffInHour);
                System.out.println("speed: " + speed);
                

                
                //Missing: Check if speed out of mode_max_speed
                
                newWaypoint.setSpeed(speed);
                waypointDao.save(newWaypoint);
                
                //save new/las waypoint
                track.setLast_waypoint(newWaypoint);
                int currDistance = track.getDistance();
                track.setDistance(currDistance+(int)waypointsDistanceInKilo);
                trackDao.save(track);
            }
            
        }
    }
    
    private double distanceInK(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        //in Kilometers
        dist = dist * 1.609344;

        return (dist);
    }
    
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @Override
    public List<Waypoint> getWaypointsByTrackId(Integer trackId) {
        return waypointDao.getWaypointsByTrackId(trackId);
    }
    
}
