/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.impl;

import at.nam.bo.ModeBo;
import at.nam.dao.ModeDao;
import at.nam.model.Mode;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nam
 */

@Service()
public class ModeBoImpl implements ModeBo{
    
    @Autowired
    ModeDao modeDao;

    @Override
    public Mode save(Mode eo) {
        return modeDao.save(eo);
    }

    @Override
    public void update(Mode eo) {
        modeDao.update(eo);
    }

    @Override
    public void delete(Mode eo) {
        modeDao.delete(eo);
    }

    @Override
    public List<Mode> getAll() {
        return modeDao.getAll();
    }

    @Override
    public Mode get(Integer id) {
        return modeDao.get(id);
    }
    
}
