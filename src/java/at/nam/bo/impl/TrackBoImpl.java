/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.impl;

import at.nam.bo.TrackBo;
import at.nam.dao.TrackDao;
import at.nam.dao.WaypointDao;
import at.nam.model.Track;
import at.nam.model.Waypoint;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nam
 */

@Service()
public class TrackBoImpl implements TrackBo{
    
    @Autowired
    TrackDao trackDao;
    
    @Autowired
    WaypointDao waypointDao;

    @Override
    public Track save(Track eo) {
        return trackDao.save(eo);
    }

    @Override
    public void update(Track eo) {
        trackDao.update(eo);
    }

    @Override
    public void delete(Track eo) {
        trackDao.delete(eo);
    }

    @Override
    public List<Track> getAll() {
        return trackDao.getAll();
    }

    @Override
    public Track get(Integer id) {
        return trackDao.get(id);
    }

    @Override
    public List<Track> getTracksByUserId(Integer userId) {
        return trackDao.getTracksByUserId(userId);
    }

    @Override
    public void endTrack(Integer trackId) {
        
        List<Waypoint> waypoints = waypointDao.getWaypointsByTrackId(trackId);
        int speedSum = 0;
            
        if ( waypoints.size() >= 3) {

            for(int i=0; i<waypoints.size(); i++) {
                speedSum += waypoints.get(i).getSpeed();
            }
        
            Track track = trackDao.get(trackId);
            track.setAvg_speed(speedSum/waypoints.size());
            trackDao.save(track);
        }
        //else delete track and waypoints
    }
}
