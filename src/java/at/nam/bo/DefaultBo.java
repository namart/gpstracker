/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo;

import java.util.List;

/**
 *
 * @author nam
 */
public interface DefaultBo<T> {
    
    T save(T eo);
    void update(T eo);
    void delete(T eo);
    List<T> getAll();
    T get( Integer id );
    
}
