/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo;

import at.nam.model.User;

/**
 *
 * @author nam
 */
public interface UserBo extends DefaultBo<User> {
    
    User AuthenticateUser(String username, String password);
    Integer getUserIdByUsername(String username);
    
}
