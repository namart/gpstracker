/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo;

import at.nam.model.Track;
import java.util.List;

/**
 *
 * @author nam
 */
public interface TrackBo extends DefaultBo<Track> {
    
    List<Track> getTracksByUserId(Integer userId);
    void endTrack(Integer trackId);
    
}
