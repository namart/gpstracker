/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo.model;


import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


/**
 *
 * @author nam
 */

public class Registration {
    
    @NotEmpty
    private String username;
    
    @NotEmpty
    @Size(min = 4, max = 20)
    private String password;
    
    @NotEmpty
    private String confirmPassword;
    
    @NotEmpty
    @Email
    private String email;
    
    @AssertTrue(message="Passwörter stimmen nicht überein")
    public boolean isRegistered() {
        return this.password.equals(this.confirmPassword);
    }
    

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    

}
