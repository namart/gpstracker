/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.bo;

import at.nam.model.Waypoint;
import java.util.List;

/**
 *
 * @author nam
 */
public interface WaypointBo extends DefaultBo<Waypoint> {
    
    void newWaypoint(Integer trackId, double latitude, double longtitude);
    List<Waypoint> getWaypointsByTrackId(Integer trackId);
    
}
