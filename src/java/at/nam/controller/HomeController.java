/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.controller;

import at.nam.bo.RoleBo;
import at.nam.bo.UserBo;
import at.nam.model.Role;
import at.nam.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author nam
 */

@Controller
public class HomeController {
    
    
    public HomeController() {
    }
    
    @RequestMapping("/home")
    protected ModelAndView home() {
        
        ModelMap model = new ModelMap();
        
     

        return new ModelAndView("home", model);
    }
    

}
