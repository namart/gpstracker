/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.controller;

import at.nam.bo.TrackBo;
import at.nam.bo.UserBo;
import at.nam.bo.WaypointBo;
import at.nam.model.Track;
import at.nam.model.Waypoint;
import java.security.Principal;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author nam
 */

@Controller
public class TrackController {
    
    protected static Logger logger = Logger.getLogger(TrackController.class);
    
    @Autowired
    private TrackBo trackBo;
    
    @Autowired
    private UserBo userBo;
    
    @Autowired
    private WaypointBo waypointBo;
    
    @RequestMapping(value = "/track", method = RequestMethod.GET)
    public ModelAndView getTracks(ModelMap model, Principal principal ) {
        
        String username = principal.getName();        
        Integer userId = userBo.getUserIdByUsername(username);
        
        List<Track> tracks = trackBo.getTracksByUserId(userId);  
        if ( tracks.size() >= 1) model.addAttribute("tracks", tracks);
        
        model.addAttribute("title", "My Tracks");
 
        return new ModelAndView("tracks", model);
    }
    
    @RequestMapping(value = "/track/{trackId}", method = RequestMethod.GET)
    public ModelAndView getTrack(@PathVariable Integer trackId, ModelMap model, Principal principal ) {
        
        String username = principal.getName();        
        Integer userId = userBo.getUserIdByUsername(username);
        
        Track track = trackBo.get(trackId);
        model.addAttribute("track", track);
        if(track != null && track.getUser().getId() == userId) {
            
            List<Waypoint> waypoints = waypointBo.getWaypointsByTrackId(trackId);
            
            if ( waypoints.size() >= 3) {
                model.addAttribute("start", waypoints.get(0));
                model.addAttribute("end", waypoints.get(waypoints.size()-1));
                
                waypoints.remove(waypoints.get(0));
                waypoints.remove(waypoints.get(waypoints.size()-1));
                model.addAttribute("waypoints", waypoints);
            }
            
            model.addAttribute("title", track.getName());
            
            return new ModelAndView("track", model);
        }
        
        
        return new ModelAndView("redirect:/home");
    }
    
}
