/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package at.nam.controller;

import at.nam.bo.RoleBo;
import at.nam.bo.UserBo;
import at.nam.bo.model.Registration;
import at.nam.model.Role;
import at.nam.model.User;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author nam
 */

@Controller
public class AccountController {
    
    @Autowired
    private UserBo userbo;
    
    @Autowired
    private RoleBo roleBo;
    
    protected static Logger logger = Logger.getLogger(AccountController.class);
    
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public ModelAndView login(ModelMap model) {

        return new ModelAndView("login");

    }
    
    @RequestMapping(value="/loginfailed", method = RequestMethod.GET)
    public String loginerror(ModelMap model) {

        model.addAttribute("error", "true");
        return "login";

    }
    
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logout(ModelMap model) {

        return "login";

    }
    
    @RequestMapping(value="/register", method = RequestMethod.GET)
    public String showRegistration(ModelMap model) {

        model.addAttribute("registration", new Registration());
        return "register";

    }
    
    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String processRegistration(@ModelAttribute("registration") @Valid Registration registration,
                        BindingResult result) {

        
        if (result.hasErrors()) {
            return "register";
        }
        
        User user = new User();
        user.setUsername(registration.getUsername());
        user.setPassword(registration.getPassword());
        user.setEmail(registration.getEmail());
        user.setEnabled(Boolean.TRUE);
        
        try {
            this.userbo.save(user);
        } catch (DataIntegrityViolationException e) {
            FieldError error = new FieldError("user", "username", registration.getUsername(), false, new String[]{e.getMessage()}, new Object[]{"argument"}, "User existiert schon");
            result.addError(error);
            return "register";
        } catch (DataAccessException e) {
            return "register";
        }
 
        
        Role role = new Role();
        role.setAuthority("user");
        role.setUser(user);
        roleBo.save(role);
        
        //return "redirect:home";
        return "registersuccess";


        

    }
}
